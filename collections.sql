CREATE OR REPLACE PROCEDURE generator(ile IN NUMBER)
IS
	licznik NUMBER(15);
	OBI_Nazwa varchar2(100);
	OBI_Liczba_pokoi number(4);
	OBI_Cena_single number(5,2);
	OBI_Cena_double number(7,2);
	ADR_Id number(4); 
	KAT_Id number(4);
	WLA_Id number(4);
	KON_Id number(4);
	
	ADR_Miejscowosc varchar2(30);
	ADR_Ulica varchar2(30);
	ADR_Numer_domu number(3);
	ADR_Numer_lokalu number(3);
	ADR_Kod_pocztowy varchar2(10);
	
	KAT_Nazwa varchar2(30);
	
	
	TYPE varray_of_varchar2 IS VARRAY(500) OF VARCHAR2(100);
	TYPE varray_of_number IS VARRAY(100) OF NUMBER(15);
	
	v_nazwa varray_of_varchar2;
	
	
	v_miejscowosc varray_of_varchar2;
	v_ulica varray_of_varchar2;
	v_kod_pocztowy varray_of_varchar2;
	v_kategoria varray_of_varchar2;
	
	adres_id number;
	kategoria_id number;
BEGIN
	
	licznik:=1;
	
	v_nazwa:= varray_of_varchar2(
	'Hôtel Saint-Jules',
	'Best Hotel',
	'Hotel Daisy',
	'Hôtel Fleur',
	'Hotel Hilton',
	'Hotel Paris',
	'Hotel Crystal',
	'Hôtel Saint Louis',
	'Prague Express',
	'Hôtel Tour Eiffel',
	'Red Tulip Hotel',
	'Gaudi Hotel', --50
	'Berlin Express',
	'City Hotel',
	'Hotel Yellow',
	'Fountain Hotel',
	'Marco Polo Express',
	'Star Hotel',
	'Lithuania Royal',
	'Medici Star Hotel',
	'Columbus Hotel',
	'Michelangelo Express',
	'Hotel Picasso',
	'Botticelli Hotel',
	'Budget Hotel',
	'Villa Hotel',
	'Marco Polo Star'
	);
	
	v_ulica:= varray_of_varchar2('ul. Romana Kielkowskiego','ul. Braci Kiemliczow','ul. Jana Kiepury','ul. Kazimierza Kierzkowskiego','ul. Kijanki','ul. Jana Kilinskiego','ul. sw. Kingi','ul. Stanislawa Klimeckiego','ul. Kliniec','ul. Sebastiana Klonowica','ul. Jana Krzysztofa Kluka','ul. Jacka Kluszewskiego','ul. Franciszka Klaka','ul. Klodzka','ul. Kluszynska','ul. Kminkowa','ul. Kobierzynska','ul. Tadeusza Kochmanskiego','ul. Kokosowa','ul. Kokotowska','ul. Kolarzy','ul. Kolejarzy','ul. Kolejowa','os. Kolejowe','ul. Kolista','ul. Kolna','ul. Kolonijna','ul. Stefana Kolaczkowskiego','ul. Kolobrzeska','ul. Kolodziejska','ul. Komandosow','ul. Komuny Paryskiej','ul. Konfederacka','ul. Marii Konopnickiej','ul. Konwaliowa','ul. Izydora Kopernickiego','ul. Koprowa','ul. Ludomila Korczynskiego','ul. Kordiana','ul. Jana Korepty','ul. Kormoranow','ul. Michala Korpala','ul. Kosiarzy','ul. Kosocicka','ul. Kazimierza Kostaneckiego','ul. Kostrzecka','ul. Jozefa Kostrzewskiego','ul. Koszalinska','ul. Koszarowka','ul. Koszutki','ul. Koszykarska','ul. Kosciuszkowcow','ul. Floriana Kotowskiego','ul. Kotowka','ul. Aleksandra Kotsisa','ul. Kowalska','ul. Kozia','ul. Kozienicka','ul. Jana Kozietulskiego','ul. Kozlarska','ul. Kajetana Kozmiana','ul. Krosnienska','ul. Krolowka','ul. Konstantego Krumlowskiego','ul. Wladyslawa Krygowskiego','ul. Krymska','ul. Krystyna z Ostrowa','ul. Krysztalowa','ul. Krzemieniecka','ul. Krzemionki','ul. Krzewowa','ul. Edmunda Krzymuskiego','ul. Krzywda','ul. Adama Krzyzanowskiego','ul. Miroslawa Krzyzanskiego','ul. Krzyztoporska','ul. Feliksa Ksiezarskiego','ul. Ksiezycowa','ul. Kukielek Golkowickich','ul. plk. Ryszarda Kuklinskiego','ul. Henryka Kulakowskiego','ul. Ferdynanda Kurasia','ul. Jana Kurczaba','ul. Karola Kurpinskiego','ul. Jerzego Kurylowicza','ul. ks. Jana Kusia','ul. gen. Jozefa Kustronia','ul. gen. Tadeusza Kutrzeby','ul. Kwatery','ul. Kwiatowa','ul. Kwiecista','ul. Kwietna','ul. Lanckoronska','ul. Lwa Landaua','ul. Jerzego Lande','ul. Lasek','ul. Laskowa','ul. Lasogorska','ul. Szlak','ul. Szpitalna');
	v_miejscowosc:= varray_of_varchar2('Arlamow','Augustow','Baranow Sandomierski','Barlinek','Belchatow','Bialystok','Biecz','Bielsko Biala','Brzeg','Busko Zdroj','Bydgoszcz','Bytow','Chorzow','Cieszyn','Czerniawa Zdroj','Czerwinsk','Czestochowa','Czorsztyn','Darlowo','Dabki','Dlugopole-Zdroj','Duszniki-Zdroj','Elblag','Elk','Frombork','Gdansk','Gdynia','Gizycko','Gliwice','Gniew','Gniezno','Goczalkowice-Zdroj','Golub-Dobrzyn','Goldap','Gorzow Wielkopolski','Grudziadz','Hel','Horyniec-Zdroj','Inowlodz','Inowroclaw','Iwonicz Zdroj','Janow Podlaski','Jastarnia','Jedlina Zdroj','Jelenia Gora','Jurata','Kalisz','Kamien Pomorski','Karpacz','Kartuzy','Katowice','Kazimierz Dolny','Katy Rybackie','Kielce','Kliczkow','Kolobrzeg','Koniakow','Konin','Konstancin Jeziorna','Koszalin','Kornik','Krakow','Krasiczyn','Krasnobrod','Krosno','Krynica Morska','Krynica Zdroj','Lezajsk','Lidzbark Warminski','Lipnica Murowana','Lubiaz','Lublin','Lagow','Lancut','Leba','Lowicz','Lodz','Rabka Zdroj','Radom','Radziejowice','Reszel','Rogalin','Rogow','Rozewie','Ruciane Nida','Rzeszow','Rymanow Zdroj','Ryn','Sieradz','Sierpc','Skarzysko-Kamienna','Skierniewice','Slupsk','Sochaczew','Solec Zdroj','Solina','Suprasl','Szczawno-Zdroj','Szczawnica','Szczecin');
	v_kod_pocztowy:= varray_of_varchar2('33-342','33-300','22-115','12-110','35-232','03-123','18-330','10-154','32-857','10-947','26-026');
	
	v_kategoria := varray_of_varchar2('hotel', 'hostel', 'schronisko', 'motel', 'pokoje goscinne', 'agroturystyka', 'apartament', 'penthouse');
		
	while licznik < ile+1
	
	LOOP
		OBI_Nazwa:= v_nazwa(ROUND(DBMS_RANDOM.VALUE(1,v_nazwa.count),0));
		
		OBI_Liczba_pokoi:=ROUND(DBMS_RANDOM.VALUE(1,100),0);
		OBI_Cena_single:= ROUND(DBMS_RANDOM.VALUE(1,100),0);
		OBI_Cena_double:= ROUND(DBMS_RANDOM.VALUE(1,100),0);
		
		ADR_Miejscowosc:=v_miejscowosc(ROUND(DBMS_RANDOM.VALUE(1,v_miejscowosc.count),0));
		ADR_Ulica:=v_ulica(ROUND(DBMS_RANDOM.VALUE(1,v_ulica.count),0));
		ADR_Numer_domu:=ROUND(DBMS_RANDOM.VALUE(1,100),0);
		ADR_Numer_lokalu:=ROUND(DBMS_RANDOM.VALUE(1,100),0);
		ADR_Kod_pocztowy:=v_kod_pocztowy(ROUND(DBMS_RANDOM.VALUE(1,v_kod_pocztowy.count),0));
		
		KAT_Nazwa:=v_kategoria(ROUND(DBMS_RANDOM.VALUE(1,v_kategoria.count),0));

		
		BEGIN
		
		
		
		INSERT INTO ADRES
		(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) 
		VALUES (ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) --100
		RETURNING ADRk_Id INTO adres_id;
		
		INSERT INTO  KATEGORIA_OBIEKTU
		(KAT_Nazwa) 
		VALUES (KAT_Nazwa)
		RETURNING KATk_Id INTO kategoria_id;
		
		INSERT INTO OBIEKT
		(OBI_Nazwa, OBI_Liczba_pokoi, OBI_Cena_single, OBI_Cena_double, ADR_Id, KAT_Id, WLA_Id, KON_Id) 
		VALUES (OBI_Nazwa, OBI_Liczba_pokoi, OBI_Cena_single, OBI_Cena_double, adres_id, kategoria_id, 1, 3);
		
        licznik:=licznik+1;
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;

	END LOOP;
END;
/


BEGIN
	generator(2);
END;
/