CREATE or REPLACE PROCEDURE INSERT_KLIENT
	(Imie IN VARCHAR2, Nazwisko IN VARCHAR2, KonId IN NUMBER,
	Miejscowosc IN VARCHAR2, Ulica IN VARCHAR2, NrDom IN NUMBER, NrLok IN NUMBER, KodPocz IN VARCHAR2)
	IS
		ADR_Id_curr ADRES.ADRk_Id%TYPE;
	BEGIN
		INSERT INTO ADRES(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) 
		VALUES(Miejscowosc, Ulica, NrDom, NrLok, KodPocz);
	--
	select SEQ_ADRES.currval INTO ADR_Id_curr from dual;
	-- 
	INSERT INTO KLIENT(KLI_Imie, KLI_Nazwisko, ADR_Id, KON_Id) 
	VALUES(Imie, Nazwisko, ADR_Id_curr, KonId);
END;
/

BEGIN
INSERT_KLIENT('Olaf', 'Garleja', 3, 'Tychy', 'Przemyslowa', 36, 17, '43-100');
END;
/


CREATE or REPLACE PROCEDURE insert_klient_petla(ile In NUMBER)
IS
	licznik number(2);
	adres varchar2(30);
	kontakt varchar2(20);
BEGIN
	licznik :=1;
	WHILE licznik < ile+1
	LOOP
		INSERT_KLIENT('Mikolaj'||licznik,'Zych'||licznik, 3, 'Zakopane', 'Kwiatowa', licznik+6, licznik, '30-50'||licznik);
	licznik := licznik+1;		
	END LOOP;
END;
/

BEGIN 
	insert_klient_petla(10);
END;
/
	
	drop procedure insert_klient_petla;

CREATE or REPLACE PROCEDURE KLI_ADRES_INSER(ile IN number)
IS

	ADR_Id_curr ADRES.ADRk_Id%TYPE;
	
	licznik number(2);
BEGIN
	licznik := 1;
	WHILE licznik < ile+1
	LOOP
	
	
		INSERT INTO ADRES
		(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy)
		VALUES('Miejscowosc'||licznik,'Ulica'||licznik,licznik,licznik,'31-000'||licznik);
		
		SELECT SEQ_ADRES.CURRVAL INTO ADR_Id_curr FROM dual;
	
		INSERT INTO KLIENT
		(KLI_IMIE,KLI_Nazwisko,ADR_Id, KON_Id)
		VALUES('Imie'||licznik,'Nazwisko'||licznik,ADR_Id_curr,1);
		licznik := licznik + 1;
	END LOOP;
END;
/

BEGIN
	KLI_ADRES_INSER(4);
END;
/

column ADR_Miejscowosc Heading 'Miejscowosc' format A15
column ADR_Ulica Heading 'Ulica' format A15
column ADR_Numer_domu Heading 'Numer_domu' format 99
column ADR_Numer_lokalu Heading 'Numer_lokalu' format 99
column ADR_Kod_pocztowy Heading 'Kod_pocztowy' format A8
SELECT * FROM ADRES;
SELECT * FROM KLIENT;


CREATE OR REPLACE PROCEDURE dodaj_kategorie(nazwa IN VARCHAR2)
IS
BEGIN
   SAVEPOINT start_tran;
   
   INSERT INTO KATEGORIA_OBIEKTU(KAT_Nazwa) VALUES (nazwa);
   
EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      ROLLBACK TO start_tran;
END;
/

BEGIN
	dodaj_kategorie('hotel');
END;
/


set serveroutput on;
CREATE OR REPLACE PROCEDURE dodaj_klienta_z_rezerwacja(miasto IN VARCHAR2, ulica IN VARCHAR2, 
numer_domu IN NUMBER, numer_lokalu IN NUMBER,  kod IN VARCHAR2, telefon IN VARCHAR2,
mail IN VARCHAR2, imie IN VARCHAR2, nazwisko IN VARCHAR2,
obiekt_nazwa IN VARCHAR2, data_roz IN VARCHAR2, data_zak IN VARCHAR2, cena IN NUMBER)
IS
	adres_id number;
	kontakt_id number;
	klient_id number;
	obiekt_id number;
BEGIN
   SAVEPOINT start_tran;
   
	INSERT INTO ADRES(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) 
		VALUES (miasto, ulica, numer_domu, numer_lokalu, kod) 
		RETURNING ADRk_Id INTO adres_id;
		
	INSERT INTO KONTAKT(KON_Telefon, KON_Email) 
		VALUES (telefon, mail)
		RETURNING KONk_Id INTO kontakt_id;
		
	INSERT INTO KLIENT(KLI_Imie, KLI_Nazwisko, ADR_Id, KON_Id) 
		VALUES (imie, nazwisko, adres_id, kontakt_id)
		RETURNING KLIk_Id INTO klient_id;
	
	SELECT OBIk_Id INTO obiekt_id FROM OBIEKT WHERE OBI_Nazwa=obiekt_nazwa;
	
	INSERT INTO REZERWACJE(REZ_Data_ropoczecia, REZ_Data_zakonczenia, REZ_Cena, KLI_Id, STA_Id, OBI_Id) 
		VALUES (TO_DATE(data_roz, 'DD/MM/YYYY'), TO_DATE(data_zak, 'DD/MM/YYYY'), cena, klient_id, 2, obiekt_id);


EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
      ROLLBACK TO start_tran;
END;
/

BEGIN
	dodaj_klienta_z_rezerwacja('Tychy', 'Sportowa', 5, 6, '23-987', '908765342', 'elo@elo.pl', 'Maciek', 'Rusek', 'Sheraton Warszawa', '08/04/2016', '11/04/2016', 2500);
END;
/
