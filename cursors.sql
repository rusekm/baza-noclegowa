CREATE OR REPLACE PROCEDURE Kursor_KLIENT
IS
	kli_imie KLIENT.KLI_Imie%TYPE;
	kli_imie_duza KLIENT.KLI_Imie%TYPE;
	kli_nazwisko KLIENT.KLI_Nazwisko%TYPE;
	kli_nazwisko_duza KLIENT.KLI_Nazwisko%TYPE;
	licznik NUMBER(2);
CURSOR KLIENT_Kursor
IS
	SELECT KLI_imie, KLI_nazwisko FROM KLIENT;
BEGIN
	OPEN KLIENT_Kursor;
	licznik := 1;
	LOOP
		FETCH KLIENT_Kursor INTO kli_imie, kli_nazwisko;
		
		EXIT WHEN KLIENT_Kursor%NOTFOUND OR KLIENT_Kursor%ROWCOUNT < 1;
		
		kli_imie_duza := INITCAP(kli_imie);
		UPDATE KLIENT SET KLI_Imie = kli_imie_duza WHERE KLIk_Id = licznik;
		kli_nazwisko_duza := INITCAP(kli_nazwisko);
		UPDATE KLIENT SET KLI_Nazwisko = kli_nazwisko_duza WHERE KLIk_Id = licznik;
		
		licznik := licznik + 1;
	END LOOP;
END;
/

INSERT INTO KLIENT(KLI_Imie, KLI_Nazwisko, ADR_Id, KON_Id) VALUES ('patryk', 'mazur', 1, 1);

SELECT * FROM KLIENT;

BEGIN
	Kursor_KLIENT();
END;
/

SELECT * FROM KLIENT;,


CREATE OR REPLACE PROCEDURE Kursor_ADRES_delete
IS
	miasto ADRES.ADR_Miejscowosc%TYPE;
BEGIN
	DELETE FROM ADRES WHERE miasto=NULL;
	IF SQL%FOUND THEN
		dbms_output.put_line('Usunieto' || to_char(SQL%ROWCOUNT || 'rekordow'));
	ELSE
		dbms_output.put_line('Brak rekordow do usuniecia');
	END IF;
END;
/

BEGIN
	Kursor_ADRES_delete();
END;
/

CREATE OR REPLACE PROCEDURE Kursor_REZERWACJE_przedluz(dataRozString IN VARCHAR2, dataZakString IN VARCHAR2)
IS
	data REZERWACJE.REZ_Data_zakonczenia%TYPE;
BEGIN
	SELECT TO_DATE(dataZakString, 'dd/mm/yyyy')
		INTO data FROM DUAL;
	UPDATE REZERWACJE SET REZERWACJE.REZ_Data_zakonczenia=TO_DATE(dataZakString, 'dd/mm/yyyy')
	WHERE REZ_Data_ropoczecia=TO_DATE(dataRozString, 'dd/mm/yyyy');
END;
/

INSERT INTO REZERWACJE(REZ_Data_ropoczecia, REZ_Data_zakonczenia, REZ_Cena, KLI_Id, STA_Id, OBI_Id) VALUES (TO_DATE('31/05/2016', 'DD/MM/YYYY'), TO_DATE('11/06/2016', 'DD/MM/YYYY'), 2500, 1, 1, 1);

BEGIN
	Kursor_REZERWACJE_przedluz('31/05/2016', '12/06/2016');
END;
/


CREATE OR REPLACE PROCEDURE Kursor_refcursor
IS
	nazwa_tabeli varchar2(40):='OBIEKT';
	kursor sys_refcursor;
	type tablica is table of OBIEKT%rowtype index by binary_integer;
	tab tablica;
BEGIN
	OPEN kursor for 'SELECT * FROM '||nazwa_tabeli;
	FETCH kursor BULK COLLECT INTO tab;
	dbms_output.put_line('Tablica zawiera: ' || tab.count || ' elementow' );
	for k in tab.first..tab.last
	loop
		dbms_output.put_line(tab(k).OBI_Nazwa || ', liczba pokoi: ' || tab(k).OBI_Liczba_pokoi);
	end loop;
	CLOSE kursor;
END;
/


BEGIN
	Kursor_refcursor;
END;
/


CREATE OR REPLACE PROCEDURE Kursor_refcursor2
IS
	kursor sys_refcursor;
	obi OBIEKT%ROWTYPE;
BEGIN
	OPEN kursor for select * FROM OBIEKT;
	LOOP
		FETCH kursor INTO obi;
		exit WHEN kursor%notfound;
		dbms_output.put_line(obi.OBI_Nazwa || '  liczba pokoi: '  || obi.OBI_Liczba_pokoi );
	END LOOP;
	CLOSE kursor;
END;
/

BEGIN
	Kursor_refcursor2;
END;
/

CREATE OR REPLACE PROCEDURE Kursor_uaktualnijPlace(nazwisko IN PRACOWNIK.PRA_Nazwisko%TYPE, 
nowa_placa PRACOWNIK.PRA_Wynagrodzenie%TYPE,
liczba OUT INTEGER)
AS
	id_Kursora INTEGER;
	Instr_Update VARCHAR2(100);

BEGIN
	id_Kursora := DBMS_SQL.OPEN_CURSOR;
	Instr_Update := ' UPDATE PRACOWNIK SET PRA_Wynagrodzenie = :np WHERE PRA_Nazwisko = :nazw';
	DBMS_SQL.PARSE(id_Kursora, Instr_Update, DBMS_SQL.NATIVE);
	DBMS_SQL.BIND_VARIABLE(id_Kursora, ':np', nowa_placa);
	DBMS_SQL.BIND_VARIABLE(id_Kursora, ':nazw', nazwisko);
	liczba := DBMS_SQL.EXECUTE(id_Kursora);

	DBMS_SQL.CLOSE_CURSOR(id_Kursora);
EXCEPTION
	WHEN OTHERS THEN
		DBMS_SQL.CLOSE_CURSOR(id_Kursora);
	RAISE;
END Kursor_uaktualnijPlace;
/


DECLARE
	liczba NUMBER;
BEGIN
	Kursor_uaktualnijPlace('Zuch', 5600, liczba);
	dbms_output.put_line(liczba);
END;
/
