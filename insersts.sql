INSERT INTO STATUS(STA_Nazwa) VALUES ('zaplacone');
INSERT INTO STATUS(STA_Nazwa) VALUES ('niezaplacone');

INSERT INTO KATEGORIA_OBIEKTU(KAT_Nazwa) VALUES ('hotel');
INSERT INTO KATEGORIA_OBIEKTU(KAT_Nazwa) VALUES ('hostel');
INSERT INTO KATEGORIA_OBIEKTU(KAT_Nazwa) VALUES ('pensjonat');
INSERT INTO KATEGORIA_OBIEKTU(KAT_Nazwa) VALUES ('schronisko');

INSERT INTO KONTAKT(KON_Telefon, KON_Email) VALUES ('879093123', 'znowak@gmail.com');
INSERT INTO KONTAKT(KON_Telefon, KON_Email) VALUES ('678094225', 'a_zuch@onet.pl');
INSERT INTO KONTAKT(KON_Telefon, KON_Email) VALUES ('543098345', 'sheraton@sheraton.pl');

INSERT INTO ADRES(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) VALUES ('Krakow', 'Szeroka', '6', '7', '30-234');
INSERT INTO ADRES(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) VALUES ('Limanowa', 'Dluga', '9', '14', '34-600');
INSERT INTO ADRES(ADR_Miejscowosc, ADR_Ulica, ADR_Numer_domu, ADR_Numer_lokalu, ADR_Kod_pocztowy) VALUES ('Warszawa', 'Piekna', '19', '17', '05-075');


INSERT INTO WLASCICIEL(WLA_Nazwa, WLA_NIP, ADR_Id, KON_Id) VALUES ('Sheraton Polska', 6570941891, 3, 3);

INSERT INTO KLIENT(KLI_Imie, KLI_Nazwisko, ADR_Id, KON_Id) VALUES ('Zbigniew', 'Nowak', 2, 1);

INSERT INTO OBIEKT(OBI_Nazwa, OBI_Liczba_pokoi, OBI_Cena_min, OBI_Cena_max, ADR_Id, KAT_Id, WLA_Id, KON_Id) VALUES ('Sheraton Warszawa', 190, 300, 2000, 3, 1, 1, 3);

INSERT INTO PRACOWNIK(PRA_Imie, PRA_Nazwisko, PRA_Wynagrodzenie, PRA_Data_rozpoczecia, ADR_Id, KON_Id, OBI_Id) VALUES ('Alojzy', 'Zuch', 2500, TO_DATE('08/04/2002', 'DD/MM/YYYY'), 1, 2, 1);

INSERT INTO REZERWACJE(REZ_Data_ropoczecia, REZ_Data_zakonczenia, REZ_Cena, KLI_Id, STA_Id, OBI_Id) VALUES (TO_DATE('08/04/2016', 'DD/MM/YYYY'), TO_DATE('11/04/2016', 'DD/MM/YYYY'), 1239, 1, 1, 1);
