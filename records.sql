CREATE OR REPLACE PROCEDURE usun_te_same_obiekty(o1_id IN number, o2_id IN number)
IS
    TYPE rek_obi IS RECORD(
        nazwa OBIEKT.OBI_Nazwa%type,
        liczba_pokoi OBIEKT.OBI_Liczba_pokoi%type,
        cena_single OBIEKT.OBI_Cena_single%type,
        cena_double OBIEKT.OBI_Cena_double%type
    );

    obiekt1 rek_obi;
    obiekt2 rek_obi;
BEGIN
    SELECT O.OBI_Nazwa, O.OBI_Liczba_pokoi, O.OBI_Cena_single, O.OBI_Cena_double
        INTO obiekt1 FROM OBIEKT O
        WHERE O.OBIk_Id = o1_id;

    SELECT O.OBI_Nazwa, O.OBI_Liczba_pokoi, O.OBI_Cena_single, O.OBI_Cena_double
        INTO obiekt2 FROM OBIEKT O
        WHERE O.OBIk_Id = o2_id;


    IF obiekt1.nazwa = obiekt2.nazwa
        AND obiekt1.liczba_pokoi = obiekt2.liczba_pokoi
        AND obiekt1.cena_single = obiekt2.cena_single
        AND obiekt1.cena_double = obiekt2.cena_double
    THEN
        DBMS_OUTPUT.PUT_LINE('Takie same obiekty');
		DELETE FROM OBIEKT
			WHERE OBIk_Id = o2_id;
		DBMS_OUTPUT.PUT_LINE('Usunieto');
        
    ELSE
        DBMS_OUTPUT.PUT_LINE('Produkty rozne');
    END IF;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Brak wystarczajacych danych');
END;
/

BEGIN
	usun_te_same_obiekty(1,4);
END;
/

CREATE OR REPLACE PROCEDURE rekordy_zagniezdzone(klient_imie IN VARCHAR2, klient_nazwisko IN VARCHAR2,
klient_telefon IN VARCHAR2, klient_email IN VARCHAR2)
IS
	TYPE kontakt_rekord IS RECORD(
		telefon KONTAKT.KON_Telefon%type,
		email KONTAKT.KON_Email%type
	);
	
	TYPE klient_rekord IS RECORD(
		imie KLIENT.KLI_Imie%type,
		nazwisko KLIENT.KLI_Nazwisko%type,
		kontakt kontakt_rekord
	);
	
	klient1 klient_rekord;
	kontakt_id number;
	
BEGIN
	klient1.imie:=klient_imie;
	klient1.nazwisko:=klient_nazwisko;
	klient1.kontakt.telefon:=klient_telefon;
	klient1.kontakt.email:=klient_email;

	INSERT INTO 
	KONTAKT(KON_Telefon, KON_Email) 
	VALUES (klient1.kontakt.telefon, klient1.kontakt.email)
	RETURNING KONk_Id INTO kontakt_id;
	
	INSERT INTO 
	KLIENT(KLI_Imie, KLI_Nazwisko, ADR_Id, KON_Id) 
	VALUES (klient1.imie, klient1.nazwisko, 2, kontakt_id);
	
	
END;
/

BEGIN
	rekordy_zagniezdzone('Jaroslaw', 'Kruk', '675452347', 'rek@rekord.pl');
END;
/