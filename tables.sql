CLEAR SCREEN;

DELETE FROM REZERWACJE;
drop table REZERWACJE;

DELETE FROM PRACOWNIK;
drop table PRACOWNIK;

DELETE FROM OBIEKT;
drop table OBIEKT;

DELETE FROM KLIENT;
drop table KLIENT;

DELETE FROM WLASCICIEL;
drop table WLASCICIEL;


DELETE FROM ADRES;
drop table ADRES;

DELETE FROM KONTAKT;
drop table KONTAKT;

DELETE FROM KATEGORIA_OBIEKTU;
drop table KATEGORIA_OBIEKTU;

DELETE FROM STATUS;
drop table STATUS;


create table
STATUS(
STAk_Id number(4) NOT NULL,
STA_Nazwa varchar2(30) NOT NULL
);

alter table STATUS add constraint CSR_PK_STATUS primary key (STAk_Id);

create table
KATEGORIA_OBIEKTU(
KATk_Id number(4) NOT NULL,
KAT_Nazwa varchar2(30) NOT NULL
);

alter table KATEGORIA_OBIEKTU add constraint CSR_PK_KATEGORIA_OBIEKTU primary key (KATk_Id);

create table
KONTAKT(
KONk_Id number(4) NOT NULL,
KON_Telefon varchar2(20),
KON_Email varchar2(20)
);

alter table KONTAKT add constraint CSR_PK_KONTAKT primary key (KONk_Id);

create table
ADRES(
ADRk_Id number(4) NOT NULL,
ADR_Miejscowosc varchar2(30),
ADR_Ulica varchar2(30),
ADR_Numer_domu number(3),
ADR_Numer_lokalu number(3),
ADR_Kod_pocztowy varchar2(10)
);

alter table ADRES add constraint CSR_PK_ADRES primary key (ADRk_Id);

create table
WLASCICIEL(
WLAk_Id number(4) NOT NULL,
WLA_Nazwa varchar2(60) NOT NULL,
WLA_NIP number(10),
ADR_Id number(4) NOT NULL,
KON_Id number(4) NOT NULL
);

alter table WLASCICIEL add constraint CSR_PK_WLASCICIEL primary key (WLAk_Id);
alter table WLASCICIEL add constraint CSR_FK1_WLASCICIEL foreign key (ADR_Id) references ADRES (ADRk_Id);
alter table WLASCICIEL add constraint CSR_FK2_WLASCICIEL foreign key (KON_Id) references KONTAKT (KONk_Id);

create table
KLIENT(
KLIk_Id number(4) NOT NULL,
KLI_Imie varchar2(30) NOT NULL,
KLI_Nazwisko varchar2(30) NOT NULL,
ADR_Id number(4) NOT NULL,
KON_Id number(4) NOT NULL
);

alter table KLIENT add constraint CSR_PK_KLIENT primary key (KLIk_Id);
alter table KLIENT add constraint CSR_FK1_KLIENT foreign key (ADR_Id) references ADRES (ADRk_Id);
alter table KLIENT add constraint CSR_FK2_KLIENT foreign key (KON_Id) references KONTAKT (KONk_Id);

create table
OBIEKT(
OBIk_Id	number(4) NOT NULL,
OBI_Nazwa varchar2(100) NOT NULL,
OBI_Liczba_pokoi number(4),
OBI_Cena_min number(5,2),
OBI_Cena_max number(7,2),
ADR_Id number(4) NOT NULL,
KAT_Id number(4) NOT NULL,
WLA_Id number(4) NOT NULL,
KON_Id number(4) NOT NULL
);

alter table OBIEKT add constraint CSR_PK_OBIEKT primary key (OBIk_Id);
alter table OBIEKT add constraint CSR_FK1_OBIEKT foreign key (ADR_Id) references ADRES (ADRk_Id);
alter table OBIEKT add constraint CSR_FK2_OBIEKT foreign key (KAT_Id) references KATEGORIA_OBIEKTU (KATk_Id);
alter table OBIEKT add constraint CSR_FK3_OBIEKT foreign key (WLA_Id) references WLASCICIEL (WLAk_Id);
alter table OBIEKT add constraint CSR_FK4_OBIEKT foreign key (KON_Id) references KONTAKT (KONk_Id);

create table
PRACOWNIK(
PRAk_Id number(4) NOT NULL,
PRA_Imie varchar2(30) NOT NULL,
PRA_Nazwisko varchar2(30) NOT NULL,
PRA_Wynagrodzenie number(7,2),
PRA_Data_rozpoczecia date,
ADR_Id number(4) NOT NULL,
KON_Id number(4) NOT NULL,
OBI_Id number(4) NOT NULL
);

alter table PRACOWNIK add constraint CSR_PK_PRACOWNIK primary key (PRAk_Id);
alter table PRACOWNIK add constraint CSR_FK1_PRACOWNIK foreign key (ADR_Id) references ADRES (ADRk_Id);
alter table PRACOWNIK add constraint CSR_FK2_PRACOWNIK foreign key (KON_Id) references KONTAKT (KONk_Id);
alter table PRACOWNIK add constraint CSR_FK3_PRACOWNIK foreign key (OBI_Id) references OBIEKT (OBIk_Id);

create table
REZERWACJE(
REZk_Id number(4) NOT NULL,
REZ_Data_ropoczecia date NOT NULL,
REZ_Data_zakonczenia date NOT NULL,
REZ_Cena number(8,2),
KLI_Id number(4) NOT NULL,
STA_Id number(4) NOT NULL,
OBI_Id number(4) NOT NULL
);

alter table REZERWACJE add constraint CSR_PK_REZERWACJE primary key (REZk_Id);
alter table REZERWACJE add constraint CSR_FK1_REZERWACJE foreign key (KLI_Id) references KLIENT (KLIk_Id);
alter table REZERWACJE add constraint CSR_FK2_REZERWACJE foreign key (STA_Id) references STATUS (STAk_Id);
alter table REZERWACJE add constraint CSR_FK3_REZERWACJE foreign key (OBI_Id) references OBIEKT (OBIk_Id);

-- sekwencje

drop sequence SEQ_STATUS;

create sequence SEQ_STATUS increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_KATEGORIA_OBIEKTU;

create sequence SEQ_KATEGORIA_OBIEKTU increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_KONTAKT;

create sequence SEQ_KONTAKT increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_ADRES;

create sequence SEQ_ADRES increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_WLASCICIEL;

create sequence SEQ_WLASCICIEL increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_KLIENT;

create sequence SEQ_KLIENT increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_OBIEKT;

create sequence SEQ_OBIEKT increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_PRACOWNIK;

create sequence SEQ_PRACOWNIK increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

drop sequence SEQ_REZERWACJE;

create sequence SEQ_REZERWACJE increment by 1 start with 1
maxvalue 9999999999 minvalue 1;

-- triggery

create or replace trigger T_BI_STATUS
before insert on STATUS
for each row
begin
	if :new.STAk_Id is NULL then
		SELECT SEQ_STATUS.nextval INTO :new.STAk_Id FROM dual;
	end if;
end;
/

create or replace trigger T_BI_KATEGORIA_OBIEKTU
before insert on KATEGORIA_OBIEKTU
for each row
begin
	if :new.KATk_Id  is NULL then
		SELECT SEQ_KATEGORIA_OBIEKTU.nextval INTO :new.KATk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_KONTAKT
before insert on KONTAKT
for each row
begin
	if :new.KONk_Id  is NULL then
		SELECT SEQ_KONTAKT.nextval INTO :new.KONk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_ADRES
before insert on ADRES
for each row
begin
	if :new.ADRk_Id  is NULL then
		SELECT SEQ_ADRES.nextval INTO :new.ADRk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_WLASCICIEL
before insert on WLASCICIEL
for each row
begin
	if :new.WLAk_Id  is NULL then
		SELECT SEQ_WLASCICIEL.nextval INTO :new.WLAk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_KLIENT
before insert on KLIENT
for each row
begin
	if :new.KLIk_Id  is NULL then
		SELECT SEQ_KLIENT.nextval INTO :new.KLIk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_OBIEKT
before insert on OBIEKT
for each row
begin
	if :new.OBIk_Id	 is NULL then
		SELECT SEQ_OBIEKT.nextval INTO :new.OBIk_Id	 FROM dual;
	end if;
end;
/

create or replace trigger T_BI_PRACOWNIK
before insert on PRACOWNIK
for each row
begin
	if :new.PRAk_Id  is NULL then
		SELECT SEQ_PRACOWNIK.nextval INTO :new.PRAk_Id  FROM dual;
	end if;
end;
/

create or replace trigger T_BI_REZERWACJE
before insert on REZERWACJE
for each row
begin
	if :new.REZk_Id  is NULL then
		SELECT SEQ_REZERWACJE.nextval INTO :new.REZk_Id  FROM dual;
	end if;
end;
/
